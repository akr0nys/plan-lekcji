<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Plan lekcji</title>
    <link rel="stylesheet" href="style.css">
    <script defer src="./script.js"></script>
</head>

<body>

    <div>
        <h1>
            Plan lekcji
            <?php

$planTygodnia = array(
    "pn" => array(
        array(
            "numer" => 0,
            "przedmiot" => "",
            "nauczyciel" => "",
            "sala" => ""
        ),
    ),
    "wt" => array(
        array(
            "numer" => 0,
            "przedmiot" => "",
            "nauczyciel" => "",
            "sala" => ""
        ),
    ),
    "sr" => array(array(
        "numer" => 0,
        "przedmiot" => "",
        "nauczyciel" => "",
        "sala" => ""
    ),
    ),
    "cz" => array(array(
        "numer" => 0,
        "przedmiot" => "",
        "nauczyciel" => "",
        "sala" => ""
    ),
    ),
    "pt" => array(array(
        "numer" => 0,
        "przedmiot" => "",
        "nauczyciel" => "",
        "sala" => ""
    ),
    )
);
            $mysqli = mysqli_connect("localhost", "plan", "haselko", "plan", 3306);
            if($mysqli == false) {
                echo "Błąd bazy danych!";
                die;
            }
            foreach(array_keys($planTygodnia) as $day)
            {
                $response = mysqli_fetch_all(mysqli_query($mysqli, 'SELECT plan.nauczyciele.Nauczyciel, plan.klasy.Nazwa AS "Klasa", 
                plan.przedmioty.Nazwa AS "Przedmiot", plan.plan.DzienTygodnia, plan.plan.Godzina, plan.sale.NrSali AS "Sala"
                FROM plan.plan 
               LEFT JOIN plan.nauczycieleprzedmioty  ON plan.nauczycieleprzedmioty.id  = plan.plan.idNP  
               left JOIN plan.nauczyciele ON plan.nauczycieleprzedmioty.idNauczyciel = plan.nauczyciele.id
               left JOIN plan.przedmioty ON plan.nauczycieleprzedmioty.idPrzedmiot  = plan.przedmioty.id 
               left JOIN plan.klasy ON plan.plan.idKlasa = plan.klasy.id 
               left  JOIN plan.sale ON plan.plan.idSala = plan.sale.id 
               WHERE plan.klasy.id = 2
               AND plan.plan.DzienTygodnia = "'.$day.'";    '));
    
    
    
                for($i = 0;$i < sizeof($response);$i++){
                $planTygodnia[$day][$i]['nauczyciel'] = $response[$i][0];
                $planTygodnia[$day][$i]['przedmiot'] = $response[$i][2];
                $planTygodnia[$day][$i]['numer'] = $response[$i][4];
                $planTygodnia[$day][$i]['sala'] = $response[$i][5];
               }
            }
            print_r(
                mysqli_fetch_all(mysqli_query($mysqli, "SELECT plan.nauczyciele.Nauczyciel, plan.klasy.Nazwa AS 'Klasa', plan.przedmioty.Nazwa AS 'Przedmiot', plan.plan.DzienTygodnia, plan.plan.Godzina, plan.sale.NrSali AS 'Sala'
                FROM plan.plan 
               LEFT JOIN plan.nauczycieleprzedmioty  ON plan.nauczycieleprzedmioty.id  = plan.plan.idNP  
               left JOIN plan.nauczyciele ON plan.nauczycieleprzedmioty.idNauczyciel = plan.nauczyciele.id
               left JOIN plan.przedmioty ON plan.nauczycieleprzedmioty.idPrzedmiot  = plan.przedmioty.id 
               left JOIN plan.klasy ON plan.plan.idKlasa = plan.klasy.id 
               left  JOIN plan.sale ON plan.plan.idSala = plan.sale.id 
               WHERE plan.klasy.id = '2'
               AND plan.plan.DzienTygodnia = 'pn';    ")),
               true
            );


            if (!isset($_GET['dzien']))
            switch (date("w")) {
                case 1:
                    $_GET['dzien'] = "pn";
                    break;
                case 2:
                    $_GET['dzien'] = "wt";
                    break;
                case 3:
                    $_GET['dzien'] = "sr";
                    break;
                case 4:
                    $_GET['dzien'] = "cz";
                    break;
                case 5:
                    $_GET['dzien'] = "pt";
                    break;
                default:
                    $_GET['dzien'] = "pn";
                    break;
            }
        $plan = $planTygodnia[$_GET['dzien']];
            ?>
        </h1>
    </div> 
    <form action="." method="get" class="mobile-only select-day" >
        <select name="dzien"  id="select">
            <option value="pn" <?php if($_GET['dzien'] == "pn") echo "selected"?>>Poniedziałek</option>
            <option value="wt" <?php if($_GET['dzien'] == "wt") echo "selected"?>>Wtorek</option>
            <option value="sr"  <?php if($_GET['dzien'] == "sr") echo "selected"?>>Środa</option>
            <option value="cz"  <?php if($_GET['dzien'] == "cz") echo "selected"?>>Czwartek</option>
            <option value="pt"  <?php if($_GET['dzien'] == "pt") echo "selected"?>>Piątek</option>
        </select>
        <button type="submit" id="submit" class="hidden">OK</button>
    </form>

    <div class="desktop-only">
        <?php
        $bufasd = array("pn", "wt", "sr", "cz", "pt");
        
        echo(' <div class="row"><div class="column kolumna-numery-lekcji">');
    for($i = 0; $i < 9; $i++)
    {
        echo(' <div class="lekcja nr-lekcji">  <div class="nr-lekcji">' . $i. '</div></div>');
    }
    echo(' </div> ');
    
    foreach($bufasd as $dzien)
    {
        echo(' <div class="column"> 
        <div class="lekcja dzien"><b>'.$dzien.'</b></div>
        ');
    
        foreach ($planTygodnia[$dzien] as &$value) {
            echo '
            <div class="lekcja">
            ';
                if ($value["przedmiot"] != "") echo '
                <div class="info">
                    <div class="przedmiot">
                    ' . $value["przedmiot"] . '
                    </div>
                    <div class="nauczyciel-i-sala">
                        <div class="nauczyciel">
                        ' . $value["nauczyciel"] . '
                        </div>
                        <div class="sala">
                        ' . $value["sala"] . '
                        </div>
                    </div>
                </div>
                ';
                echo '
            </div>';
        }
        echo(' </div> ');
    }
    
    echo(' </div> ');
    
       
        ?>
    </div>
<div class="mobile-only">
    <?php

    foreach ($plan as &$value) {
        echo '
        <div class="lekcja">
            <div class="nr-lekcji">
                ' . $value["numer"] . '
            </div>
            ';
            if ($value["przedmiot"] != "") echo '
            <div class="info">
                <div class="przedmiot">
                ' . $value["przedmiot"] . '
                </div>
                <div class="nauczyciel-i-sala">
                    <div class="nauczyciel">
                    ' . $value["nauczyciel"] . '
                    </div>
                    <div class="sala">
                    ' . $value["sala"] . '
                    </div>
                </div>
            </div>
            ';
            else echo '<div class="info ninja"></div>';
        echo '
        </div>

        ';
    }
    ?>
</div>

</body>

</html>