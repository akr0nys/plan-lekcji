-- MariaDB dump 10.19  Distrib 10.9.4-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: plan
-- ------------------------------------------------------
-- Server version	10.9.4-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `klasy`
--

DROP TABLE IF EXISTS `klasy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `klasy` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Nazwa` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klasy`
--

LOCK TABLES `klasy` WRITE;
/*!40000 ALTER TABLE `klasy` DISABLE KEYS */;
INSERT INTO `klasy` VALUES
(1,'4C WET'),
(2,'4C INF');
/*!40000 ALTER TABLE `klasy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nauczyciele`
--

DROP TABLE IF EXISTS `nauczyciele`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nauczyciele` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Nauczyciel` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nauczyciele`
--

LOCK TABLES `nauczyciele` WRITE;
/*!40000 ALTER TABLE `nauczyciele` DISABLE KEYS */;
INSERT INTO `nauczyciele` VALUES
(1,'Diana Walawender-Drela'),
(2,'Stachera Arkadiusz '),
(3,'Bukowczan Łukasz '),
(4,'Tkacz Piotr'),
(5,'The G.O.A.T.'),
(6,'Marchewka Bartosz'),
(7,'Hanulok Dominika '),
(8,'Korzeniowski Hubert '),
(9,'Kurpanik-Wójcik Aneta'),
(10,'Rolski Tytus'),
(11,'Jura Adam '),
(12,'Janda Beata');
/*!40000 ALTER TABLE `nauczyciele` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nauczycieleprzedmioty`
--

DROP TABLE IF EXISTS `nauczycieleprzedmioty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nauczycieleprzedmioty` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idNauczyciel` bigint(20) unsigned NOT NULL,
  `idPrzedmiot` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`),
  KEY `idNauczyciel` (`idNauczyciel`),
  KEY `idPrzedmiot` (`idPrzedmiot`),
  CONSTRAINT `nauczycieleprzedmioty_ibfk_1` FOREIGN KEY (`idNauczyciel`) REFERENCES `nauczyciele` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `nauczycieleprzedmioty_ibfk_2` FOREIGN KEY (`idPrzedmiot`) REFERENCES `przedmioty` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nauczycieleprzedmioty`
--

LOCK TABLES `nauczycieleprzedmioty` WRITE;
/*!40000 ALTER TABLE `nauczycieleprzedmioty` DISABLE KEYS */;
INSERT INTO `nauczycieleprzedmioty` VALUES
(1,1,1),
(2,2,2),
(3,3,3),
(4,4,4),
(5,5,5),
(6,6,6),
(7,6,7),
(8,6,8),
(9,2,9),
(10,4,10),
(11,7,11),
(12,8,12),
(13,9,13),
(14,10,14),
(15,11,15),
(16,12,16),
(17,1,17);
/*!40000 ALTER TABLE `nauczycieleprzedmioty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plan`
--

DROP TABLE IF EXISTS `plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `DzienTygodnia` tinytext NOT NULL,
  `Godzina` bigint(20) NOT NULL,
  `idKlasa` bigint(20) unsigned NOT NULL,
  `idNP` bigint(20) unsigned DEFAULT NULL,
  `idSala` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `idKlasa` (`idKlasa`,`idNP`,`idSala`),
  KEY `idSala` (`idSala`),
  KEY `idNP` (`idNP`),
  CONSTRAINT `plan_ibfk_1` FOREIGN KEY (`idKlasa`) REFERENCES `klasy` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `plan_ibfk_2` FOREIGN KEY (`idSala`) REFERENCES `sale` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `plan_ibfk_3` FOREIGN KEY (`idNP`) REFERENCES `nauczycieleprzedmioty` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan`
--

LOCK TABLES `plan` WRITE;
/*!40000 ALTER TABLE `plan` DISABLE KEYS */;
INSERT INTO `plan` VALUES
(1,'pn',0,2,NULL,NULL),
(2,'pn',1,2,1,1),
(3,'pn',2,2,2,2),
(4,'pn',3,2,3,3),
(5,'pn',4,2,4,4),
(6,'pn',5,2,4,4),
(7,'pn',6,2,5,5),
(8,'pn',7,2,2,2),
(9,'pn',8,2,NULL,NULL),
(10,'wt',0,2,NULL,NULL),
(11,'wt',1,2,6,6),
(12,'wt',2,2,6,6),
(13,'wt',3,2,6,6),
(14,'wt',4,2,7,6),
(15,'wt',5,2,7,6),
(16,'wt',6,2,8,6),
(17,'wt',7,2,8,6),
(18,'wt',8,2,9,2),
(19,'sr',0,2,NULL,NULL),
(20,'sr',1,2,10,4),
(21,'sr',2,2,10,4),
(22,'sr',3,2,10,4),
(23,'sr',4,2,11,7),
(24,'sr',5,2,12,8),
(25,'sr',6,2,13,1),
(26,'sr',7,2,5,5),
(27,'sr',8,2,14,9),
(28,'cz',0,2,NULL,NULL),
(29,'cz',1,2,NULL,NULL),
(30,'cz',2,2,NULL,NULL),
(31,'cz',3,2,NULL,NULL),
(32,'cz',4,2,5,5),
(33,'cz',5,2,12,8),
(34,'cz',6,2,NULL,NULL),
(35,'cz',7,2,NULL,NULL),
(36,'cz',8,2,NULL,NULL),
(37,'pt',0,2,16,10),
(38,'pt',1,2,16,10),
(39,'pt',2,2,16,10),
(40,'pt',3,2,1,1),
(41,'pt',4,2,15,11),
(42,'pt',5,2,1,1),
(43,'pt',6,2,17,1),
(44,'pt',7,2,14,9),
(45,'pt',8,2,NULL,NULL);
/*!40000 ALTER TABLE `plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `przedmioty`
--

DROP TABLE IF EXISTS `przedmioty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `przedmioty` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Nazwa` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `przedmioty`
--

LOCK TABLES `przedmioty` WRITE;
/*!40000 ALTER TABLE `przedmioty` DISABLE KEYS */;
INSERT INTO `przedmioty` VALUES
(1,'j. Polski'),
(2,'j. Angielski'),
(3,'Historia'),
(4,'LSBD'),
(5,'Matematyka'),
(6,'TSiAI'),
(7,'WiAI'),
(8,'ADiM'),
(9,'j. Ang. Zaw.'),
(10,'TiABD'),
(11,'j. Niemiecki'),
(12,'Geografia'),
(13,'Chemia'),
(14,'j. Ang. Roz.'),
(15,'Fizyka'),
(16,'Wych. Fiz.'),
(17,'ZZW');
/*!40000 ALTER TABLE `przedmioty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale`
--

DROP TABLE IF EXISTS `sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `NrSali` tinytext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale`
--

LOCK TABLES `sale` WRITE;
/*!40000 ALTER TABLE `sale` DISABLE KEYS */;
INSERT INTO `sale` VALUES
(1,'401'),
(2,'501a'),
(3,'408'),
(4,'205'),
(5,'408a'),
(6,'105'),
(7,'502'),
(8,'303'),
(9,'501'),
(10,'siłownia'),
(11,'409');
/*!40000 ALTER TABLE `sale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'plan'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-07 14:09:37
